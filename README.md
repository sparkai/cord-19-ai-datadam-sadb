# CORD-19 AI DataDAM #



### What is CORD-19 AI DataDAM? ###

CORD-19 AI DataDAM (CADD) is a text and data mining tool to help researchers gain insight from growing coronavirus literature.

### What can CORD-19 AI DataDAM do? ###

**DataDAM** is a digital asset management tool for datasets that stores, shares, and collates assets in a central location.
These assets include search tools, curated datasets and ML models, notebooks, visualizations, documents, and videos.

**CORD-19 AI DataDAM (CADD)** is an AI tool specifically for **CORD-19**, a document resource about **COVID-19**, 
**SARS-CoV-2**, and related coronaviruses.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact